#!/bin/bash

# Adding gruvbox colorscheme to .vim/colors/
git clone https://github.com/morhetz/gruvbox.git
mkdir -p ~/.vim/colors/
mv gruvbox/colors/gruvbox.vim ~/.vim/colors/
rm -rf gruvbox/

# Adding dotfiles to system
mv config/dotfiles/vimrc ~/.vimrc
mv config/dotfiles/xinitrc ~/.xinitrc
mv config/dotfiles/tmux.conf ~/.tmux.conf
rm -rf config
